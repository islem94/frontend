import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PaiementService } from '../paiement.service';
import { FactureService } from '../facture.service';
import { Paiement } from '../paiement';
import { Router } from '@angular/router';
import { Input, Output, EventEmitter } from '@angular/core';
import { sharedData } from '../ShaderService/sharedData';
import { Facture } from '../facture';
@Component({
  selector: 'app-paiement',
  templateUrl: './paiement.component.html',
  styleUrls: ['./paiement.component.css']
})
export class PaiementComponent implements OnInit {
  factures: Facture[];
  paiement: Paiement = new Paiement();
  submitted = false;
  typePaiement: any;
  dateP: any;
  connexion: any;
  facture: any;
  appParentMessage: string;
  resteAPayer: number;
  statut: string;
  sharedData: sharedData;
  reference: string;
  montant: number;
  montPai = 0;
  tva: any;
  prixHT: any;
  prov: string;
  dateE: string;
  montantR: any;
  dateCompare1: string;
  dateCompare2: string
  idd: any;
  tst: any = {};
  myReff = '';
  //  refFacture: any;
  constructor(private paiementService: PaiementService,private router: Router, sharedData: sharedData) {
    this.sharedData = sharedData;
    this.sharedData.currentMessage.subscribe(ref => this.reference = ref);
    this.sharedData.currentProv.subscribe(ref => this.prov = ref);
    this.sharedData.currentMontant.subscribe(ref => this.montant = ref);
    this.sharedData.currentprixHT.subscribe(ref => this.prixHT = ref);

    this.sharedData.currenttva.subscribe(ref => this.tva = ref);
    this.sharedData.currentDateEcheance.subscribe(ref => this.dateE = ref);
    this.sharedData.currentMontantPaiement.subscribe(ref=>this.montantPaiement =ref);
    this.sharedData.id.subscribe(val => { this.idd = val })

  }


  /* receiveMessage($event) {
     this.appParentMessage = $event
   }*/


  ngOnInit() {
    //this.paiement.statutPaiement = ""

  }

  newPaiement(): void {
    this.paiement.refPaiement = this.reference;

    this.submitted = false;
    this.paiement = new Paiement();
    this.paiement.statutPaiement = this.statut;
    this.paiement.refPaiement = this.reference;
    this.paiement.provider = this.prov;
    this.paiement.prixHT = this.prixHT;
    this.paiement.tva = this.tva;
    this.paiement.dateEcheanceP = this.dateE;
    this.paiement.montantPaiement=this.montantPaiement;
    this.paiement.montantP = this.montant;
    this.paiement.montantR = this.montantR;
    console.log("---------------");
    console.log(this.montantR);

    if (this.montantR != 0) {
      this.paiement.statutPaiement = 'Partiel';

    }
    else if (this.montantR == 0) {
      this.paiement.statutPaiement = 'Soldée';

    }
  else 
  this.paiement.statutPaiement= 'ENREGISTREE'
  }

  save() {
    console.log(this.myReff)
    this.montantR = this.montant - this.paiement.montantPaiement;
    this.paiement.montantR = this.montantR;
    this.paiement.prixHT = this.prixHT;
    this.paiement.tva = this.tva;
  
    console.log("*********")
    console.log(this.paiement)
   
      this.paiementService.filterByRf(this.reference).subscribe(val => {
        console.log(val);
        this.tst = val;
          if(this.tst && this.tst.idPaiement){
             this.paiementService.editPaiement(this.paiement, this.idd).subscribe(val=>{

             },
             error => console.log(error))
             this.paiement = new Paiement();
          }
          else{
            this.paiementService.editAndSavePaiement(this.paiement, this.idd).subscribe(val=>{

            },
            error => console.log(error))
            this.paiement = new Paiement();
          }
      })




  }

  onSubmit() {
    this.submitted = true;
    this.save()
    console.log('ccc')


  }
  montantPaiement: any;
  functionname() { this.montantR = this.montant - this.paiement.montantPaiement }

   goToBoard(){
    this.router.navigateByUrl('/findbyProvider');

   }
}
