import { LoginComponent } from './login/login.component';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ReactiveFormsModule, FormControl, FormsModule } from '@angular/forms';

@NgModule({
  imports: [ReactiveFormsModule],
  declarations: [
    LoginComponent
  ]
})
export class AuthenticationModule{

}
