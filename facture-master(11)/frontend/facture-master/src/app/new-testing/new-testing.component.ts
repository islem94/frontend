import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PaiementService } from '../paiement.service';
import { FactureService } from '../facture.service';
import { Paiement } from '../paiement';
import { Router } from '@angular/router';
import { Input,Output, EventEmitter } from '@angular/core';
import { Fournisseur } from '../fournisseur';
import { FournisseurService } from '../fournisseur.service';


@Component({
  selector: 'app-new-testing',
  templateUrl: './new-testing.component.html',
  styleUrls: ['./new-testing.component.css']
})
export class NewTestingComponent implements OnInit {

  fournisseur: Fournisseur = new Fournisseur();
  submitted = false;
  typePaiement:any;
  dateP: any;
  connexion:any;
  facture:any;


  constructor(private fournisseurService: FournisseurService , router: Router ) { }

  ngOnInit() {
this.test()
  
    }


    newFournisseur(): void {
      this.submitted = false;
      this.fournisseur = new Fournisseur();
    }

    save() {
      this.fournisseurService.createFournisseur(this.fournisseur)
        .subscribe(data => console.log(data), error => console.log(error));
      this.fournisseur = new Fournisseur();
    }
    test(){
      this.fournisseurService.getfournisseursList().subscribe(val=>{
        console.log(val)
      })
     }
    onSubmit() {
      this.submitted = true;
      this.test()
      this.save();
    }

  /*  public setRef (val:any ):void {
      return  this.ref ;
    }

    public getRef ():any {
      return  this.ref ;
    }*/


  /*  public setRef(val:any):void{
      this.refFacture=val ;
    }

    public getRef():any{
      return this.refFacture ;
    }*/
  }
