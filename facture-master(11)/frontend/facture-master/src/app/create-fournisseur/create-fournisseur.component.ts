
import { Component, OnInit ,Input } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { Fournisseur } from '../fournisseur';
import { FournisseurService } from '../fournisseur.service';

@Component({
  selector: 'app-create-fournisseur',
  templateUrl: './create-fournisseur.component.html',
  styleUrls: ['./create-fournisseur.component.css']
})
export class CreateFournisseurComponent implements OnInit {

 //factures : Facture[];
    fournisseur: Fournisseur = new Fournisseur();
    submitted = false;

  constructor( private fournisseurService: FournisseurService , router: Router ) { }

  ngOnInit() {

  }

    newFournisseur(): void {
      this.submitted = false;
      this.fournisseur = new Fournisseur();
    }

    save() {
      this.fournisseurService.createFournisseur(this.fournisseur)
        .subscribe(data => console.log(data), error => console.log(error));
      this.fournisseur = new Fournisseur();
    }

    onSubmit() {
      this.submitted = true;
      this.save();
    }

}
