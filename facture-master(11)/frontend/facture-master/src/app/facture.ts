
export class Facture {
  idFacture: number;
  refFacture: string;
  fournisseur: string;
   prixTTC: number;
   prixHT: number;
   tva: number ; 
   statut: string ;
   date: string;
   dateEcheance: String;
   pdf:string ; 
}
