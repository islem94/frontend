import { Router } from '@angular/router';
import { Component, OnInit , ViewChild} from '@angular/core';
import { PaiementService } from '../paiement.service';
import { Paiement } from '../paiement';
import { sharedData } from '../ShaderService/sharedData';
import { Facture } from '../facture';
import { FormControl } from '@angular/forms';
import { FournisseurService } from '../fournisseur.service';


@Component({
  selector: 'app-search-paiement',
  templateUrl: './search-paiement.component.html',
  styleUrls: ['./search-paiement.component.css']
})
export class SearchPaiementComponent implements OnInit {

  rff = new FormControl('');
  fournisseur: any = [];
  paiements : any = [];
  allStatus = ['PARTIEL','ENREGISTREE','APAYER']

//paiements: Paiement[];
factures: Facture[];
provider: any ; 
statutPaiement : any ; 
dateEcheanceP : any ;
dateE :   string ; 
fournisseurs:any =[];
//prixHT: any ; 

constructor(private dataService: PaiementService , private router: Router ,private sharedData:sharedData, 
  private fourServ: FournisseurService ) { 
  /*this.sharedData=sharedData;
    this.sharedData.currentDateEcheance.subscribe(ref => this.dateE = ref);*/}

ngOnInit() {
  this.fourServ.getfournisseursList().subscribe((val=>{
    this.fournisseurs=val;
    console.log(this.fournisseur)
  }))

  this.dataService.getAllPayments().subscribe((val=>{
    this.paiements = val;
   
  }))
}

private searchFournisseur() {
  console.log(this.provider)
  this.dataService.getPaiementsByProvider(this.provider)
    .subscribe(paiements => {
      this.paiements = paiements
      console.log(this.paiements)
    })

}

private searchStatut() {
  this.dataService.getPaiementsByStatut(this.statutPaiement)
    .subscribe(paiements => this.paiements = paiements);
}


private searchDateEcheance() {
  this.dataService.getPaiementsByDateEcheance(this.dateEcheanceP)
    .subscribe(paiements => this.paiements = paiements);
}



onSubmit() { 
    this.searchFournisseur();
    this.searchStatut(); 
    this.searchDateEcheance(); 
    
}
goToPage(ref:string,amount:number , provid : string , paiement , montantPaiement:number , prixHT : number){
 // console.log(paiement)
  this.router.navigateByUrl('/paiement');
  this.sharedData.changeMontant(amount);
  this.sharedData.changeMessage(ref);
  this.sharedData.changeProv(provid);
  this.sharedData.changeId(paiement.idPaiement);
  this.sharedData.changeMontantPaiement(montantPaiement);
  this.sharedData.changeprixHT(prixHT); 
  console.log(amount);
  console.log(ref);
}



  searchByStatut(stat){
    this.dataService.filterBySatut(stat).subscribe(val=>{
      this.paiements = val;
      console.log(val)
    })
    }

    searchByRef(){
      
    this.dataService.filterByRef(this.rff.value).subscribe(val=>{
      this.paiements = val;
      console.log(val)
    })
    }
    searchByFour(four){
    
      this.dataService.filterByFour(four).subscribe(val=>{
        this.paiements = val;
        console.log(val)
      })
      }


      searchByDate(){
        var current_date=new Date();
        var timestamp = current_date.getTime();
        var formatted_date = current_date.getDate() + "/" + (current_date.getMonth()+ 1)  + "/" + current_date.getFullYear()
        console.log(formatted_date)
        this.dataService.filterByDate(this.dateEcheanceP).subscribe(val=>{
          this.paiements = val;
          console.log(val)
        })

      }

      searchByProv(prov){
    
        this.dataService.filterByProv(prov).subscribe(val=>{
          this.paiements = val;
          console.log(val)
        })

      }

      searchBySt(st){
    
        this.dataService.filterBySt(st).subscribe(val=>{
          this.paiements = val;
          console.log(val)
        })

      }

      getTotalCost() {
        return this.factures.map(t => t.prixHT).reduce((acc, value) => acc + value, 0);
      }

      showDate(){
        console.log(this.dateEcheanceP)
        console.log(this.dateEcheanceP.getDate()+'/'+this.dateEcheanceP.getMonth()+'/'+this.dateEcheanceP.getYear())
        var current_date=new Date();
        var timestamp = current_date.getTime();
        var formatted_date = current_date.getDate() + "/" + (current_date.getMonth()+ 1)  + "/" + current_date.getFullYear()
        console.log(formatted_date)
      }

      count(){
        let tot = 0;
        for(let el of this.paiements){
          tot = tot+el.montantP;
        }
        return tot;
      }

      count2(){
        let tot = 0;
        for(let el of this.paiements){
          tot = tot+el.montantR;
        }
        return tot;
      }

      count3(){
        let tot = 0;
        for(let el of this.paiements){
          tot = tot+el.tva;
        }
        return tot;
      }
      count4(){
        let tot = 0;
        for(let el of this.paiements){
          tot = tot+el.montantPaiement;
        }
        return tot;
      }
      
      count5(){
        let tot = 0;
        for(let el of this.paiements){
          tot = tot+el.prixHT;
        }
        return tot;
      }
}


