import { Component, ViewChild, ElementRef , OnInit, NgModule } from '@angular/core';
import { Router } from '@angular/router';
import * as jsPDF from 'jspdf';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CommonModule } from "@angular/common";

import {sharedData} from '../ShaderService/sharedData';
import { Facture } from '../facture';
import { FactureService } from '../facture.service';
import { ProductService } from '../product/shared/service/product.service';
import { FournisseurService } from '../fournisseur.service';
import { Fournisseur } from '../fournisseur';

@NgModule({
  imports: [
    CommonModule
  ]
}) 
@Component({
  selector: 'app-create-facture',
  templateUrl: './create-facture.component.html',
  styleUrls: ['./create-facture.component.css']
})

export class CreateFactureComponent implements OnInit {
 // pdfSrc: string = '/pdf-test.pdf';
facture: Facture = new Facture();
  submitted = false;
  fournisseur:any =[];
  date:any;
  fact = Facture;
  constructor(private factureService: FactureService , router: Router,
     private fourServ: FournisseurService) { 
  
  }

  ngOnInit() {
  this.facture.statut = "ENREGISTREE"
  this.fourServ.getfournisseursList().subscribe((val=>{
    this.fournisseur=val;
    console.log(this.fournisseur)
  }))
  }

  newFacture(): void {
    this.submitted = false;
    this.facture = new Facture();
    this.facture.statut = "ENREGISTREE"
  }

  save() {

    this.factureService.CreateFacture(this.facture)
      .subscribe(data => console.log(data), error => console.log(error));
    this.facture = new Facture();
  }




  onSubmit( dateE : string) {
    this.submitted = true;
   this.save();
  
  
  }
  
   upload(event) {
    const file = event.target.files[0];}


}
