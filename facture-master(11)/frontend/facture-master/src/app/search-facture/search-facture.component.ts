import { Router } from '@angular/router';
import { Component, OnInit , ViewChild} from '@angular/core';
import {sharedData} from '../ShaderService/sharedData';
import { Facture } from '../facture';
import { FactureService } from '../facture.service';
import { FournisseurService } from '../fournisseur.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-search-facture',
  templateUrl: './search-facture.component.html',
  styleUrls: ['./search-facture.component.css']
})

export class SearchFactureComponent implements OnInit {
  
  [x: string]: any;
  allStatus = ['ENREGISTREE']
  vm: { ref: string, prixHT: string }=
      { "ref": "0", "prixHT": "Available" };
fournisseurB : any ; 
rff = new FormControl('');
dateEcheance:any;
statut: any;
refer = "";
fournisseur: any = [];
factures: any = [];
four: any;
ref:any;
refFacture:any;
prixHT: number;
prixTTC: any ;
sharedData: sharedData;
reference:string;
montant:number;
prov:string ; 
dateE : string ; 
tva : number ; 
total: any;
constructor(private dataService: FactureService , private router: Router , sharedData: sharedData, private fourServ: FournisseurService) {
  this.sharedData=sharedData;
  this.sharedData.currentMessage.subscribe(ref => this.reference = ref);
  this.sharedData.currentProv.subscribe(ref => this.prov = ref);
  this.sharedData.currentMontant.subscribe(ref => this.montant = ref);
  this.sharedData.currentprixHT.subscribe(ref => this.prixHT = ref);
  this.sharedData.currenttva.subscribe(ref => this.tva = ref);
  this.sharedData.currentDateEcheance.subscribe(ref => this.dateE = ref);


  this.total= this.factures.prixHT + this.factures.prixTTC ;
}

ngOnInit() {
  this.searchFournisseur();
  //this.searchStatut();
  this.fourServ.getfournisseursList().subscribe((val=>{
    this.fournisseur=val;
    console.log(this.fournisseur)

  }))

}

private searchFournisseur() {
  this.dataService.getFacturesByFournisseur(this.fournisseur)
    .subscribe(factures => this.factures = factures); 
      //this.test = factures;
      //console.log(this.test);
      
    }

private searchStatut() {
  this.dataService.getFacturesByStatut(this.statut)
    .subscribe(factures => this.factures = factures);

  }
private searchRef() {
  this.dataService.getFacturesByRef(this.ref)
    .subscribe(factures => this.factures = factures);
}

private searchDateEcheance() {
  this.dataService.getFacturesByDateEcheance(this.dateEcheance)
    .subscribe(factures => this.factures = factures);
}

private searchMontantHT() {
  this.dataService.getFacturesByPrixHT(this.prixHT)
    .subscribe(factures => this.factures = factures);
}

onSubmit() {
  
  this.searchFournisseur();
  this.searchStatut();
  this.searchRef();
   // this.searchFournisseur();
    this.searchDateEcheance();
    this.searchMontantHT();

}

 goToPage(ref:string, provid : string  , amount:number  ,prixHT: number , tva: number , dateE : string ){
this.router.navigateByUrl('/paiement');
this.sharedData.changeMessage(ref);
this.sharedData.changeProv(provid);
this.sharedData.changeMontant(amount);
this.sharedData.changeprixHT(prixHT);
this.sharedData.changetva(tva); 
this.sharedData.changeDateEcheance(dateE);
console.log(amount);
console.log(ref);

}

onSelect (value){
  console.log(value); 
  }

/*private getFacture(id_Facture){
  console.log(id_Facture)
  this.dataService.getFacturee()
  .subscribe((data: Facture[])=>{
    this.factures=data ; 
 })

  var ref = this.factures.filter((word =>word.idFacture== id_Facture))
 ref.forEach( refFacture=>{
  this.cnxService.setFacture(refFacture.refFacture)
  this.router.navigateByUrl('/paiement');
 })
}
*/

/*private getFactureId(idFacture){
   console.log(idFacture)
  //this.dataService
  .getFactureId()
  .subscribe((data: Facture[])=>{
      // console.log(data);
    this.factures=data;
  })

  //  var refFacture=this.factures.filter((word =>word.id==idPaiement))
    refFacture.forEach(refFacture => {
      this.cnxService.setRef(refFacture.refFacture)

//  this.router.navigateByUrl(`facture/${idFacture}`)
  //  this.router.navigate(['/factures/${idFacture}'])
      this.router.navigateByUrl(`/paiement`);
})*/
  /*  setRef(val:any):void{
        this.refFacture=val ;
      }*/
    //}*/

    searchByFour(four){
    
    this.dataService.filterByFour(four).subscribe(val=>{
      this.factures = val;
      console.log(val)
    })
    }

    searchByStatut(stat){
    //this.tva=0; 
    //this.prixHT=0;
   
    this.dataService.filterBySatut(stat).subscribe(val=>{
      this.factures = val;
      console.log(val)
      console.log("hello"+JSON.stringify(this.factures));
    //Total*********************************************
    //  for( let i=0; i<this.factures.length; i++){
      //  this.prixTTC=this.factures[i].prixTTC +this.prixTTC;
      //  this.prixHT=this.factures[i].prixHT +this.prixHT;
      //  this.tva=this.factures[i].tva +this.tva;
      //}
    })
    }

    searchByRef(){
      
    this.dataService.filterByRef(this.rff.value).subscribe(val=>{
      this.factures = val;
      console.log(val)
    })
    }


 
}
