import { NgModule} from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { ProductComponent } from './product/product.component';
import { ProductResolver } from './product/shared/service/product.resolver.service';
import { UserResolver } from './user/shared/service/user.resolver.service';
import { LoginComponent } from './authentication/login/login.component';
import { HomeComponent } from './home/home.component';
import { UserComponent } from './user/user.component';
import { PaiementComponent } from './paiement/paiement.component';
import { FactureListComponent } from './facture-list/facture-list.component';
import { CreateFactureComponent } from './create-facture/create-facture.component';
import { SearchFactureComponent } from './search-facture/search-facture.component';
import { NewTestingComponent } from './new-testing/new-testing.component';
import { CreateFournisseurComponent } from './create-fournisseur/create-fournisseur.component';
import { SearchPaiementComponent } from './search-paiement/search-paiement.component';

export const appRoutes: Routes = [
  {  path: 'login',  component: LoginComponent},
  {  path: 'home',   component: HomeComponent},
  {  path: '',  redirectTo: '/home',  pathMatch: 'full' },
  {  path: 'product', component: ProductComponent,
   resolve: {
      products: ProductResolver
    }
  },
  {  path: 'paiement', component: PaiementComponent},
 
  { path: 'factureList', component: FactureListComponent },
     { path: 'addFact', component: CreateFactureComponent },
     { path: 'findbyfournisseur', component: SearchFactureComponent },
     { path: 'findbyProvider', component: SearchPaiementComponent },
     {  path: 'testing', component: NewTestingComponent},
     {  path: 'newFour', component: CreateFournisseurComponent},

  { path: 'user', component: UserComponent,
    resolve: {
      users: UserResolver
    }
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: true}
    )
  ],

exports: [RouterModule],
providers: [ProductResolver, UserResolver]
})
export class AppRoutingModule{

}
