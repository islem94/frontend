import { Component, OnInit, Input } from '@angular/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FactureService } from '../facture.service';
import { FactureListComponent } from '../facture-list/facture-list.component';
import { Facture } from '../facture';
@Component({
  selector: 'app-facture-details',
  templateUrl: './facture-details.component.html',
  styleUrls: ['./facture-details.component.css']
})
export class FactureDetailsComponent implements OnInit {


@Input()
facture: Facture;
pageFactures: any ; 

constructor(private factureService: FactureService, private listComponent: FactureListComponent) { }

ngOnInit() {

}

/*doSearch () {
  this.factureService.getFacture(this.idFacture)
  .subscribe(data=>{
    this.pageFactures=data;
  }, err=>{
    console.log(err);
  
  })
}*/

deleteFacture() {
  this.factureService.deleteFacture(this.facture.idFacture)
    .subscribe(
      data => {
        console.log(data);
        this.listComponent.reloadData();
      },
      error => console.log(error));
}

}
