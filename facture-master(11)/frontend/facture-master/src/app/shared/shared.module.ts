import { NgModule } from '@angular/core'
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { CrudComponent } from './crud/crud.component';
import { SampleComponent } from './crud/sample/sample.component';
//import { PayComponent } from './crud/pay/pay.component';



@NgModule({
  imports: [ReactiveFormsModule, BrowserModule],
  declarations: [
    CrudComponent,
    SampleComponent

  ],
  exports: [CrudComponent,
  SampleComponent  //PayComponent
  ]
})
export class SharedModule{

}
