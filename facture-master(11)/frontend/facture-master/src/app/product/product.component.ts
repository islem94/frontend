import {Component, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import {ProductService} from './shared/service/product.service';
import {Product} from './shared/model/product.model';
import { DataModel } from '../shared/data.model';
import { CrudService } from '../shared/crud.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  products: Product[];

  productForm: FormGroup;

  product = new Product();

  productsModel: DataModel[];

  listFournisseurs : any = [];

  constructor(private productService: ProductService, private fb: FormBuilder, private route: ActivatedRoute){

  }

  ngOnInit(){
    this.products = this.route.snapshot.data.products;
    this.productForm = this.fb.group({
      ref: ['', Validators.required],
      fournisseur: '',
      prixTTC: '',
      prixHT: '',
    //  statut:'',
      date:''

    });

    this.productsModel = [
      new DataModel('id','ID','number',true,[]),
      new DataModel('ref','Référence','string',false,[]),
      new DataModel('fournisseur','fournisseur','string',false,[]),
      new DataModel('prixTTC','prixTTC','number',false,[]),
      new DataModel('prixHT','prixHT','number',false,[]),
  //    new DataModel('statut','statut','string',false,[]),
      new DataModel('date','Date Facture','string',false,[])
    ]

  }
}
