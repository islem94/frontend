
export class Product{
  constructor(public id?: number,
              public ref?: string,
              public fournisseur?: string,
              public prixTTC?: number,
              public prixHT?: number,
            ){

  }
}
