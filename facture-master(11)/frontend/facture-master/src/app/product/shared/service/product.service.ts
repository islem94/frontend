import { Injectable } from '@angular/core';
import { CrudService } from '../../../shared/crud.service';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ProductService {
  url = '/produit';
  constructor(public http: HttpClient) { }

  getAllFournisseurs(){
    return this.http.get('api/listFournisseurs')
  }
}

