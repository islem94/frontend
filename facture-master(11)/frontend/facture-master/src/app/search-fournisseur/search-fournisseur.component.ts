import { Router } from '@angular/router';
import { Component, OnInit , ViewChild} from '@angular/core';
import { Facture } from '../facture';
import { FactureService } from '../facture.service';
@Component({
  selector: 'app-search-fournisseur',
  templateUrl: './search-fournisseur.component.html',
  styleUrls: ['./search-fournisseur.component.css']
})
export class SearchFournisseurComponent implements OnInit {
  fournisseur: any;
  factures: Facture[];
  ref:any;
  refFacture:any;


constructor(private dataService: FactureService , private router: Router ) {}

ngOnInit() {

}

private searchFournisseur() {
  this.dataService.getFacturesByFournisseur(this.fournisseur)
    .subscribe(factures => this.factures = factures);
}


onSubmit() {
    this.searchFournisseur();
   
}
private goToPage(pageName:string){
  this.router.navigateByUrl('/paiement')
}



}
