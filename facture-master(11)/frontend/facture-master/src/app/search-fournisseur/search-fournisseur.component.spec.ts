import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchFournisseurComponent } from './search-fournisseur.component';

describe('SearchFournisseurComponent', () => {
  let component: SearchFournisseurComponent;
  let fixture: ComponentFixture<SearchFournisseurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchFournisseurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchFournisseurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
