import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Paiement } from './paiement';


@Injectable({
  providedIn: 'root'
})
export class PaiementService {


  private baseUrl = 'http://localhost:8080/api/paiements';


  constructor(private http: HttpClient) { }


  getAllPayments() {
    return this.http.get(`${this.baseUrl}`);
  }

  getPaiement(idPaiement: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${idPaiement}`);
  }

  createPaiement(paiement: Object): Observable<Object> {

    return this.http.post(`${this.baseUrl}` + `/create`, paiement);
  }
  editPaiement(paiement: Object, id): Observable<Object> {

    return this.http.post(`${this.baseUrl}` + `/edit/` + id, paiement);
  }

  editAndSavePaiement(paiement: Object, id): Observable<Object> {

    return this.http.post(`${this.baseUrl}` + `/editAndSave/` + id, paiement);
  }

  updatePaiement(idPaiement: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${idPaiement}`, value);
  }

  deletePaiement(idPaiement: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${idPaiement}`, { responseType: 'text' });
  }

  getPaiementsList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  getPaiementsByTypePaiement(typePaiement: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/typePaiement/${typePaiement}`);
  }

  deleteAll(): Observable<any> {
    return this.http.delete(`${this.baseUrl}` + `/delete`, { responseType: 'text' });
  }
  getPaiementsByProvider(provider: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/provider/${provider}`);
  }

  getPaiementsByStatut(statutPaiement: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/statutPaiement/${statutPaiement}`);
  }

  getPaiementsByDateEcheance(dateEcheanceP: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/dateEcheanceP/${dateEcheanceP}`);
  }

  filterByFour(four) {
    return this.http.get(`${this.baseUrl}/getByFour/${four}`);
  }


  filterBySatut(stat) {
    return this.http.get(`${this.baseUrl}/getByStat/${stat}`);
  }


  filterByRef(ref) {
    return this.http.get(`${this.baseUrl}/getByRef/${ref}`);
  }

  filterByDate(date) {
    return this.http.post(`${this.baseUrl}/getByDate`, date);
  }

  filterByProv(prov) {
    return this.http.get(`${this.baseUrl}/getByProv/` + prov);
  }

  filterBySt(prov) {
    return this.http.get(`${this.baseUrl}/getByStat/` + prov);
  }

  filterByRf(rf) {
    return this.http.get(`${this.baseUrl}/getByRf/` + rf);
  }
}
