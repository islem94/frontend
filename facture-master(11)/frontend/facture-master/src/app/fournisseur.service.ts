import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class FournisseurService {

  private baseUrl = 'http://localhost:8080/api/listFournisseurs';
  private baseUrl2 = 'http://localhost:8080/api/fournisseurs';

  constructor(private http: HttpClient ) { }

  getFournisseur(idFournisseur: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${idFournisseur}`);
  }

  createFournisseur(fournisseur: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl2}` + `/create`, fournisseur);
  }
  updateFournisseur(idFournisseur: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${idFournisseur}`, value);
  }

  deleteFournisseur(idFournisseur: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${idFournisseur}`, { responseType: 'text' });
  }

  getfournisseursList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }


  deleteAll(): Observable<any> {
    return this.http.delete(`${this.baseUrl}` + `/delete`, { responseType: 'text' });
  }
}
