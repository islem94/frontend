import { Component, OnInit } from '@angular/core';
import { FactureService } from '../facture.service';
import { Router } from '@angular/router';
import { Facture } from '../facture';

export interface Transaction {
  fournisseur: string;
  reference: number;
  

}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent  
{
  
data: string[] = ['one', 'two', 'three', 'four', 'five'];
}