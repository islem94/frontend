import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Moment } from 'moment';

@Injectable({
  providedIn: 'root'
})
export class FactureService {

  private baseUrl = 'http://localhost:8080/api/factures';

  constructor(private http: HttpClient) { }

  getFacture(idFacture: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${idFacture}`);
  }

  getFacturee(): Observable<Object> {
    return this.http.get(`${this.baseUrl}`);
  }


/*  getFactureId(idFacture: number): Observable<Object> {
    return this.http.get(`${this.baseUrl}/${idFacture}`);
  }*/
  CreateFacture(facture: Object): Observable<Object> {
    return this.http.post(`${this.baseUrl}` + `/create`, facture);
  }

  updateFacture(idFacture: number, value: any): Observable<Object> {
    return this.http.put(`${this.baseUrl}/${idFacture}`, value);
  }

  deleteFacture(idFacture: number): Observable<any> {
    return this.http.delete(`${this.baseUrl}/${idFacture}`, { responseType: 'text' });
  }

  getFacturesList(): Observable<any> {
    return this.http.get(`${this.baseUrl}`);
  }

  getFacturesByFournisseur(fournisseur: string): Observable<any> {

    return this.http.get(`${this.baseUrl}/fournisseur/${fournisseur}`);
  }

  getFacturesByStatut(statut: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/statut/${statut}`);
  }

  getFacturesByRef(refFacture: string): Observable<any> {
    return this.http.get(`${this.baseUrl}/refFacture/${refFacture}`);
  }

  getFacturesByDateEcheance(dateEcheance: string): Observable<any> {
      return this.http.get(`${this.baseUrl}/dateEcheance/${dateEcheance}`);
    }

    getFacturesByPrixHT(prixHT: number): Observable<any> {
        return this.http.get(`${this.baseUrl}/prixHT/${prixHT}`);
      }

  deleteAll(): Observable<any> {
    return this.http.delete(`${this.baseUrl}` + `/delete`, { responseType: 'text' });
}

getAllFactures() {
  return this.http.get(`${this.baseUrl}`+'/factures')
}

filterByFour(four){
  return this.http.get(`${this.baseUrl}/getByFour/${four}`);
}


filterBySatut(stat){
  return this.http.get(`${this.baseUrl}/getByStat/${stat}`);
}


filterByRef(ref){
  return this.http.get(`${this.baseUrl}/getByRef/${ref}`);
}

}
