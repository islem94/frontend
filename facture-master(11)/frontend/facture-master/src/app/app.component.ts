import { Router } from '@angular/router';
import { Component, ViewChild, ElementRef  , OnInit } from '@angular/core';
import * as jspdf from 'jspdf';
import { AppService } from './authentication/shared/service/app.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  showHideSideBar: boolean = false;

  constructor(private appService: AppService,
              private router: Router){}

  ngOnInit(){
    if(!this.appService.authenticated){
      this.router.navigate(['/login']);
    }
    else {
      this.router.navigate(['/home']);
    }
  }

  onShowSideBarChange(showHideSideBar){
    this.showHideSideBar = showHideSideBar;
  }

}
