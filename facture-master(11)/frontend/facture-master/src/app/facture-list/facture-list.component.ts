import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { Facture } from '../facture';
import { FactureService } from '../facture.service';

@Component({
  selector: 'app-facture-list',
  templateUrl: './facture-list.component.html',
  styleUrls: ['./facture-list.component.css']
})

export class FactureListComponent implements OnInit {

  factures: Observable<Facture[]>;
   serviceload = false ;
   serachTerm : string ;
   prixHT:any;
  constructor(private factureService: FactureService) { }

  ngOnInit() {
    this.reloadData();
      this.serviceload = true ;



  }



  onSubmit() {
       this.searchMontantHT();}
  deleteFactures() {
    this.factureService.deleteAll()
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log('ERROR: ' + error));
  }

  reloadData() {
    this.factures = this.factureService.getFacturesList();
  }


  private searchMontantHT() {
    this.factureService.getFacturesByPrixHT(this.prixHT)
      .subscribe(factures => this.factures = factures);
  }
}
