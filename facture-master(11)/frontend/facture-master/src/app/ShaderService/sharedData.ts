import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class sharedData {

  private reference = new BehaviorSubject('');
  currentMessage = this.reference.asObservable();
  changeMessage(newValue: string) {
    this.reference.next(newValue)
  } 

  private montant = new BehaviorSubject(0);
  currentMontant = this.montant.asObservable();
  changeMontant(newValue: number) {
    this.montant.next(newValue)
  } 


  private prov = new BehaviorSubject('');
  currentProv = this.prov.asObservable();
  changeProv(newValue: string) {
    this.prov.next(newValue)
  } 


  private dateE = new BehaviorSubject('');
  currentDateEcheance = this.dateE.asObservable();
  changeDateEcheance(newValue: string) {
    this.dateE.next(newValue)
  } 

  private tva = new BehaviorSubject(0);
  currenttva = this.tva.asObservable();
  changetva(newValue: number) {
    this.tva.next(newValue)
  } 

  
  private idPaiment = new BehaviorSubject(0);
  id = this.idPaiment.asObservable();
  changeId(newValue: number) {
    this.idPaiment.next(newValue)
  } 


  private prixHT = new BehaviorSubject(0);
  currentprixHT = this.prixHT.asObservable();
  changeprixHT(newValue: number) {
    this.prixHT.next(newValue)
  } 

  private montantPaiement = new BehaviorSubject(0);
  currentMontantPaiement = this.montantPaiement.asObservable(); 
  changeMontantPaiement(newValue: number){
    this.montantPaiement.next(newValue)
  }

  private pdf = new BehaviorSubject('');
  currentpdf = this.pdf.asObservable(); 
  changepdf(newValue: string){
    this.pdf.next(newValue)
  }

  constructor() {}
}