import { BrowserModule } from '@angular/platform-browser';
import {CommonModule} from '@angular/common';

import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { CookieService } from 'ngx-cookie-service';
import { StoreModule } from '@ngrx/store';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { AppMaterialModule } from './app.material.module';
import { XhrInterceptor } from './authentication/xhr.interceptor';
import { principalReducer } from './authentication/shared/principal.reducer';
import { AuthenticationModule } from './authentication/authentication.module';
import { AppMenuModule } from './menu/app.menu.module';
import { ProductModule } from './product/product.module';
import { UserModule } from './user/user.module';
import { SharedModule } from './shared/shared.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule  } from '@angular/forms';
import { PaiementComponent } from './paiement/paiement.component';
import { HomeComponent } from './home/home.component';
//import { ExtractComponent } from './extract/extract.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
//import { FactureComponent } from './facture/facture.component';
import { CreateFactureComponent } from './create-facture/create-facture.component';
import { SearchFactureComponent } from './search-facture/search-facture.component';
import { FactureListComponent } from './facture-list/facture-list.component';
import { CreateFournisseurComponent } from './create-fournisseur/create-fournisseur.component';
import {MaterialModule} from './material.module';
import {MatTableFilterFormModule} from 'ngx-mat-table-extensions';
import { NewTestingComponent } from './new-testing/new-testing.component';
import { FournisseurComponent } from './fournisseur/fournisseur.component';
import { SearchFournisseurComponent } from './search-fournisseur/search-fournisseur.component';
import { sharedData } from './ShaderService/sharedData';
import { SearchPaiementComponent } from './search-paiement/search-paiement.component';
import { TableFilterPipe } from './home/table-filter.pipe';
import { FactureDetailsComponent } from './facture-details/facture-details.component';
import { MatInputModule } from '@angular/material/input';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  PaiementComponent,
  CreateFactureComponent,
  SearchFactureComponent,
  FactureListComponent,
  FactureDetailsComponent,
  CreateFournisseurComponent,
  NewTestingComponent
  , TableFilterPipe, FournisseurComponent
  , SearchFournisseurComponent,
   SearchPaiementComponent,


  ],
  imports: [
    PdfViewerModule,
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    AuthenticationModule,
    StoreModule.forRoot({principal: principalReducer}),
    AppMenuModule,
    ProductModule,
    UserModule,
    SharedModule,
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    Ng2SearchPipeModule,
    MaterialModule,
    MatTableFilterFormModule,
    AppMaterialModule,
    MatInputModule,
    

  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: XhrInterceptor, multi: true},
    CookieService,sharedData
  ],
  bootstrap: [AppComponent ]
})
export class AppModule { }
